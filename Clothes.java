public class Clothes{
	
	private int clothesSize;
	private int clothesPrice;
	private String clothesColour;
	
	
	public Clothes(int clothesSize, int clothesPrice, String clothesColour) {
		this.clothesSize = clothesSize;
		this.clothesPrice = clothesPrice;
		this.clothesColour = clothesColour;
	}
	
	

	public int getclothesSize(){
		return this.clothesSize;
	}
	
	public int getclothesPrice(){
		return this.clothesPrice;
	}
	
	public String getclothesColour(){
		return this.clothesColour;
	}
	
	
	public void setclothesPrice(int clothesPrice){
		this.clothesPrice = clothesPrice; 
	}
	
	public void setclothesColour(String clothesColour){
		this.clothesColour = clothesColour;
	}


}


	

